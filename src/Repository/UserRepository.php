<?php

namespace App\Repository;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(
        RegistryInterface $registry,
        UserHandler $userHandler)
    {
        parent::__construct($registry, User::class);
        $this->userHandler = $userHandler;
    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return User|null
     */
    public function getByCredentials(string $plainPassword, string $email)
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.password = :password')
                ->andWhere('a.email = :email')
                ->setParameter(
                    'password',
                    $this->userHandler->encodePlainPassword($plainPassword)
                )
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getSocialData(string $social_id)
    {
        try {
            return $this->createQueryBuilder('t')
                ->select('t')
                ->where('t.vkId = :social_id')
                ->orWhere('t.faceBookId =:social_id')
                ->orWhere('t.googleId =:social_id')
                ->setParameter('social_id', $social_id)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
