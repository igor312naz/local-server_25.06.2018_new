<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 19.06.18
 * Time: 19:48
 */

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class uLoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/register-case", name="app-register-case")
     */
    public function registerCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = new User();
        $user->setEmail($userData['email']);

        $form = $this->createForm(
            'App\Form\ULoginRegisterType', $user, [
                'action' => $this
                    ->get('router')
                    ->generate('app-register-case2')
            ]
        );
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя
        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm(
            'App\Form\ULoginRegisterType',
            $user
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setPassword("social------------------------" . time());
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("index");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('registration/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/auth-case", name="app-auth-case")
     * @param UserRepository $userRepository
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     * @throws ApiException
     */
    public function authCaseAction(
        UserRepository $userRepository,
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = null;

        $user = $userRepository->getSocialData($userData['uid']);

        if ($user) {
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('index');
        }

        $cent = $apiContext->getClientByUid(
            $userData['network'], $userData['uid']);


        try {
            if ($cent) {
                $centralData = $apiContext->getClientByEmail($userData['email']);

                $user = $userHandler->createNewUser(
                    $centralData,
                    false
                );
                $manager->persist($user);
                $manager->flush();
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('index');
            } else {
                $error = 'Ты не тот, за кого себя выдаешь';
            }
        } catch (ApiException $e) {
            $error = 'Что-то где-то пошло нетак';
        }

        return $this->render(
            '/sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);

    }


    /**
     * @Route("/join", name="join")
     *
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws ApiException
     */
    public function joinCaseAction(
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = $this->getUser();

        if ($userData['network'] == 'facebook') {

            $user->setFaceBookId($userData['uid']);
        } elseif ($userData['network'] == 'vkontakte') {

            $user->setVkId($userData['uid']);
        } elseif ($userData['network'] == 'google') {

            $user->setGoogleId($userData['uid']);
        }

        if ($user) {
            $data = $user->__toArray();

            $apiContext->linkNetworkWithClient($data);

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('profile');
        }

        return $this->redirectToRoute('index');
    }



    /**
     * @Route("/profile", name="profile")
     *
     *
     */
    public function profileAction()
    {
        $user = $this->getUser();
        $socNet = [];
        if ($user->getFaceBookId() == null) {
            $socNet[] = 'facebook';
        }
        if ($user->getVkId() == null) {
            $socNet[] = 'vkontakte';
        }
        if ($user->getGoogleId() == null) {
            $socNet[] = 'google';
        }
        if (count($socNet) > 0) {
            $socNet = implode(',', $socNet);
        }

        return $this->render('profile.html.twig', [
            'user' => $this->getUser(),
            'soc_net' => $socNet
        ]);
    }

}